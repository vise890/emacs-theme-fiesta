(defun disable-all-custom-enabled-themes ()
  (-each custom-enabled-themes 'disable-theme ))
(defun load-theme! (theme)
  (progn
    (disable-all-custom-enabled-themes)
    (load-theme theme t)))

(defun broken-theme? (theme)
  ;; dammit subatomic256!!
  (or (eq 'subatomic theme)
      (eq 'zonokai theme)
      (eq 'leuven theme)
      (eq 'xresources theme)
      (eq 'badger theme)
      (eq 'tommyh theme)
      (eq 'apropospriate theme)
      (eq 'zweilight theme)
      (eq 'hemisu theme)
      (eq 'solarized theme)
      (eq 'tao theme)))
(defun custom-available-themes-good ()
  (->> (custom-available-themes)
       (-remove 'broken-theme?)))

(defun take-screenshot (output-postfix)
  (let ((output (concat "./tmp/screenshot-" (symbol-name output-postfix) ".png")))
    (progn
      (shell-command (concat "scrot --focussed " output))
      output)))
(defun screenshot-available-themes ()
  (-each (custom-available-themes-good)
    (lambda (theme)
      (progn
        (message (concat "loading: " (symbol-name theme)))
        (load-theme! theme)
        (sit-for 1)
        (take-screenshot theme)))))
(defun cycle-available-themes ()
  (-each (custom-available-themes-good)
    (lambda (theme)
      (progn
        (message (concat "loading: " (symbol-name theme)))
        (load-theme! theme)
        (sit-for 5)))))

(defun package-desc (package) (cadr package))
(defun package-name (package) (car package))
(defun package-keywords (package) (package-desc--keywords (package-desc package)))
(defun has-theme-kws? (package)
  (-any? (lambda (k)
           (or (string= "theme" k)
               (string= "color" k)))
         (package-keywords package)))

(defun has-theme-name? (package)
  (string-match-p "\\(^color-theme\\|-themes?$\\)"
                  (symbol-name (package-name package))))

(defun theme? (package)
  (or (has-theme-name? package)
      (has-theme-kws? package)))

(defun theme-kws-but-non-theme-name? (package)
  (and (not (has-theme-name? package))
       (has-theme-kws? package)))

(defun package-names-matching (package-pred)
  (->> package-archive-contents
       (-filter package-pred)
       (-map 'package-name)))

(defun packages-list-themes ()
  (package-names-matching 'theme?))

(defun packages-list-themes-with-non-theme-name? ()
  (package-names-matching 'theme-with-non-theme-name?))

(defun packages-install (packages)
  (-each packages
    (lambda (pkg)
      (unless (package-installed-p pkg)
        (message (concat "installing pkg: " (symbol-name pkg)))
        (package-install pkg)))))
(defun packages-install-all-themes ()
  (packages-install (packages-list-themes)))

(defun theme-fiesta-start ()
  (progn
    (packages-install-all-themes)
    (disable-all-custom-enabled-themes)
    (screenshot-available-themes)))

(lambda playground ()

  (package-refresh-contents)

  (packages-list-themes)
  (length (packages-list-themes))

  (packages-list-themes-with-non-theme-name?)

  (length (custom-available-themes))

  (custom-available-themes-good)
  (length (custom-available-themes-good))

  (disable-all-custom-enabled-themes)

  (packages-install-all-themes)

  (screenshot-available-themes)
  (cycle-available-themes)

  )

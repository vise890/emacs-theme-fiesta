melpa_mirror_url = https://www.mirrorservice.org/sites/melpa.org/packages/

.PHONY: build run scrape_melpa_themes

default:


build:
	docker build -t emacs-theme-fiesta:latest .

run:
	mkdir -p tmp
	docker run --rm --interactive --tty -v $$(pwd)/tmp:/root/tmp emacs-theme-fiesta bash

scrape_melpa_themes:
	wget \
		--recursive \
		--no-parent \
		--no-clobber \
		--accept-regex 'theme' \
		--reject-regex '.txt$$' \
		$(melpa_mirror_url)

themes.txt: scrape_melpa_themes
	find ./www.mirrorservice.org/ -type f \
		| xargs --max-args 1 basename \
		> $@

clean:
	rm -rf ./www.mirrorservice.org ./themes

FROM debian:stretch
MAINTAINER Martino Visintin

RUN echo "deb http://deb.debian.org/debian stretch contrib" >> /etc/apt/sources.list && \
  apt-get -y update && apt-get -y dist-upgrade && \
  apt-get -y install \
    xvfb dbus-x11 \
    emacs25 \
    fonts-firacode \
    scrot

WORKDIR "/root"
ADD bashrc .bashrc

CMD ["bash"]

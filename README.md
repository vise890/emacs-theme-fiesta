# `emacs-theme-fiesta`

I like Emacs colour themes and I cannot lie. But finding good ones is a pain.

So. The plan is to make a gallery of ALL themes that are and ever will be on Melpa.


# TODO

1. [x] Run GUI Emacs in a container (using `Xvfb`);
1. [ ] Write [some elisp](./theme-fiesta.el) to:
   - [ ] list all themes available from Melpa;
   - [ ] install them all! HOOAH!;
   - [ ] run through them, one by one, and take a screenshot of one (with `scrot`);
1. [ ] Use [`hugo-gallery`](https://github.com/icecreammatt/hugo-gallery) to make a gallery;
1. [ ] Run this whole thing on Gitlab CI;
1. [ ] Publish to Gitlab Pages;
1. [ ] Enormous profit.
